#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import codecs
import sys
import random

WORDS_COUNT = 140


def create_mimic_dict(filename):
    with codecs.open(filename, "r", encoding="utf-8") as f:
        text = f.read()

    words = text.replace('\n', ' ').split()
    words = map(lambda x: x.strip(), words)
    words = filter(lambda x: x != '', words)
    words = list(words)

    mimic_dict = {'': [words[0]]}

    for i in range(len(words) - 1):
        word = words[i]
        next_word = words[i + 1]

        if word in mimic_dict:
            mimic_dict[word].append(next_word)
        else:
            mimic_dict[word] = [next_word]

    return mimic_dict


def print_random_text(mimic_dict):
    first_key = ''
    word_key = first_key
    text_words = []

    for i in range(WORDS_COUNT):
        if word_key not in mimic_dict:
            word_key = first_key

        word_key = random.choice(mimic_dict[word_key])
        text_words.append(word_key)

    text = ' '.join(text_words)
    print(text)


def main():
    if len(sys.argv) != 2:
        print('usage: ./mimic.py file-to-read')
        sys.exit(1)

    filename = sys.argv[1]
    mimic_dict = create_mimic_dict(filename)
    print_random_text(mimic_dict)


if __name__ == '__main__':
    main()
