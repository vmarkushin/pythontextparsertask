#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import codecs
import string
import sys


def get_words_dictionary(filename):
    with codecs.open(filename, "r", encoding="utf-8") as f:
        text = f.read()

    words = text.split()
    words = map(lambda x: x
                .strip(string.punctuation + " \t")
                .lower(), words)
    words = filter(lambda x: x != '', words)

    words_dict = {}
    for w in words:
        words_dict[w] = words_dict.get(w, 0) + 1

    return words_dict


def print_words(filename):
    words_dict = get_words_dictionary(filename)

    for key in sorted(words_dict.keys()):
        print("%s: %s" % (key, words_dict[key]))


def print_top(filename):
    words_dict = get_words_dictionary(filename)
    sorted_items = sorted(words_dict.items(), key=lambda x: x[1])

    for tup in list(reversed(sorted_items))[:20]:
        print("%s: %s" % tup)


def main():
    if len(sys.argv) != 3:
        print('usage: python wordcount.py {--count | --topcount} file')
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]

    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)
    else:
        print('unknown option: ' + option)
    sys.exit(1)


if __name__ == '__main__':
    main()
